import { http } from './config'

export default {

    listar:() => {
        return http.get('conta')
    },

    salvar:(conta) => {
        return http.post('conta', conta)
    },

    atualizar:(conta) => {
        return http.put('conta', conta)
    },

    apagar:(conta) => {
        return http.delete('conta', { data: conta })
    }

}