import { http } from './config'

export default {

    listar:() => {
        return http.get('lancamento')
    },

    salvar:(lancamento) => {
        return http.post('lancamento', lancamento)
    },

    atualizar:(lancamento) => {
        return http.put('lancamento', lancamento)
    },

    apagar:(lancamento) => {
        return http.delete('lancamento', { data: lancamento })
    }

}